import boto3
ec2 = boto3.resource('ec2')

instances = ec2.instances.filter(
	Filters=[{'Name': 'instance-state-name', 'Values': ['running']}])
for instance in instances:
    for tag in instance.tags:
        if tag['Key'] == 'Name':
            boxName = tag['Value']
            if boxName == 'CaliforniaBox':
                print("That is not dead which can eternal lie. \nIt is called %s" % boxName + ".")
            else:
                print("California isn't dreaming.")
